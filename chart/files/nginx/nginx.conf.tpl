###############################################################################

pid              /var/run/nginx.pid;
user             nginx;
worker_processes {{ .Values.workers }};

###############################################################################

error_log  /var/log/nginx/error.log warn;

###############################################################################

events {
    worker_connections  1024;
}

###############################################################################

include /etc/nginx/conf.d/*.main.conf;

###############################################################################

{{- if .Values.http.enabled }}

http {

    ###########################################################################

    include      /etc/nginx/mime.types;
    default_type application/octet-stream;

    ###########################################################################

    log_format http '$remote_addr - $remote_user [$time_local] "$request" '
                    '$status $body_bytes_sent "$http_referer" '
                    '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log http;

    ###########################################################################

    sendfile                  on;
    tcp_nopush                on;
    tcp_nodelay               on;
    reset_timedout_connection on;

    ###########################################################################

    include /etc/nginx/conf.d/*.http.conf;

    ###########################################################################

    include /etc/nginx/conf.d/*.http.server.conf;

    ###########################################################################

}

###############################################################################

{{- end }}
{{- if .Values.stream.enabled }}

stream {

    ###########################################################################

    log_format stream '$remote_addr [$time_local] '
                      '$protocol $status $bytes_sent $bytes_received '
                      '$session_time';

    access_log /var/log/nginx/access.log stream;

    ###########################################################################

    tcp_nodelay on;

    ###########################################################################

    include /etc/nginx/conf.d/*.stream.conf;

    ###########################################################################

    include /etc/nginx/conf.d/*.stream.server.conf;

    ###########################################################################

}

###############################################################################

{{- end }}
