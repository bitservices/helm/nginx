###############################################################################

server {

    ###########################################################################

    listen {{ include "nginx.stream.port" . }}{{if (and .Values.stream.server.tls.cert .Values.stream.server.tls.key) }} ssl{{ end }};

    ###########################################################################
{{- if (and .Values.stream.server.tls.cert .Values.stream.server.tls.key) }}

    ssl_certificate     {{ .Values.stream.server.tls.cert }};
    ssl_certificate_key {{ .Values.stream.server.tls.key }};

    ###########################################################################
{{- end }}
{{- if .Values.stream.server.config }}

{{ tpl .Values.stream.server.config . | indent 4 }}

    ###########################################################################
{{- end }}

}

###############################################################################
