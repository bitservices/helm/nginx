###############################################################################

server {

    ###########################################################################

    listen {{ include "nginx.http.port" . }} default_server{{ if (and .Values.http.server.tls.cert .Values.http.server.tls.key) }} ssl{{ end }};
    server_name _;

    ###########################################################################
{{- if (and .Values.http.server.tls.cert .Values.http.server.tls.key) }}

    ssl_certificate     {{ .Values.http.server.tls.cert }};
    ssl_certificate_key {{ .Values.http.server.tls.key }};

    ###########################################################################
{{- end }}

    root   /var/www/html;
    index  index.html;

    ###########################################################################

    location /healthz {
        if ($isHealthProbe != 1) {
          return 404;
        }

        add_header Content-Type text/plain;
        return 200 'IMOK';
    }

    ###########################################################################
{{- if .Values.http.server.config }}

{{ tpl .Values.http.server.config . | indent 4 }}

    ###########################################################################
{{- end }}

}

###############################################################################
