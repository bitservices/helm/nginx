{{/* vim: set filetype=mustache: */}}
{{/* --------------------------------------------------------------------- */}}
{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "nginx.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{/* --------------------------------------------------------------------- */}}
{{/*
Generate standard set of labels to be used throughout the chart.
*/}}
{{- define "nginx.labels" -}}
{{- range $k, $v := .Values.labels }}
{{ $k }}: "{{ tpl $v $ }}"
{{- end -}}
{{- end -}}
{{/* ---------------------------------------------------------------------- */}}
{{/*
Generate standard selector to be used throughout the chart.
*/}}
{{- define "nginx.selector" -}}
{{- range $k, $v := .Values.selector }}
{{ $k }}: "{{ tpl $v $ }}"
{{- end -}}
{{- end -}}
{{/* ---------------------------------------------------------------------- */}}
{{/*
Generate standard affinity to be used throughout the chart.
*/}}
{{- define "nginx.affinity" -}}
{{- range $k, $v := .Values.labels }}
- key: "{{ $k }}"
  operator: In
  values:
    - "{{ tpl $v $ }}"
{{- end -}}
{{- end -}}
{{/* ---------------------------------------------------------------------- */}}{{/*
Generate port to use for HTTP/HTTPS block.
*/}}
{{- define "nginx.http.port" -}}
{{- if .Values.http.port -}}
{{- .Values.http.port -}}
{{- else -}}
{{- if (and .Values.http.server.tls.cert .Values.http.server.tls.key) -}}
{{- printf "%d" 443 -}}
{{- else -}}
{{- printf "%d" 80 -}}
{{- end -}}
{{- end -}}
{{- end -}}
{{/* --------------------------------------------------------------------- */}}
{{/*
Generate port to use for TCP/UDP stream block.
*/}}
{{- define "nginx.stream.port" -}}
{{- if .Values.stream.port -}}
{{- .Values.stream.port -}}
{{- else -}}
{{- if (and .Values.stream.server.tls.cert .Values.stream.server.tls.key) -}}
{{- printf "%d" 49153 -}}
{{- else -}}
{{- printf "%d" 49152 -}}
{{- end -}}
{{- end -}}
{{- end -}}
{{/* --------------------------------------------------------------------- */}}
